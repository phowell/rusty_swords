use crate::class;

pub const DEFAULT_CLASSES: [class::Class; 12] = [
    class::Class {
        name: "Barbarian",
        description: "A fierce warrior of primitive background who can enter a battle rage",
        hit_die: 12,
    },
    class::Class {
        name: "Bard",
        description: "An inspiring magician whose power echoes the music of creation",
        hit_die: 8,
    },
    class::Class {
        name: "Cleric",
        description: "A priestly champion who wields divine magic in service of a higher power",
        hit_die: 8,
    },
    class::Class {
        name: "Druid",
        description: "A priest of the Old Faith, wielding the powers of nature -
        moonlight and plant growth, fire and lightning - and adopting animal forms",
        hit_die: 8,
    },
    class::Class {
        name: "Fighter",
        description: "A master of martial combat, skilled with a variety of weapons and armour",
        hit_die: 10,
    },
    class::Class {
        name: "Monk",
        description: "A master of martial arts, harnessing the power of the body
        in pursuit of physical and spiritual perfection",
        hit_die: 8,
    },
    class::Class {
        name: "Paladin",
        description: "A holy warrior bound to a sacred oath",
        hit_die: 10,
    },
    class::Class {
        name: "Ranger",
        description: "A warrior who uses martial prowess and nature magic
         to combat threats on the edges of civilization",
        hit_die: 10,
    },
    class::Class {
        name: "Rogue",
        description: "A scoundrel who uses stealth and trickery to overcome obstacles and enemies",
        hit_die: 8,
    },
    class::Class {
        name: "Sorcerer",
        description: "A spellcaster who draws on inherent magic from a gift or bloodline",
        hit_die: 6,
    },
    class::Class {
        name: "Warlock",
        description: "A wielder of magic that is derived from a
         bargain with an extraplanar entity",
        hit_die: 8,
    },
    class::Class {
        name: "Wizard",
        description: "A scholarly magic user capable of
         manipulating the structures of reality",
        hit_die: 6,
    },
];
