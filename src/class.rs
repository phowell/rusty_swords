use crate::default_classes;

pub struct Class {
    pub name: &'static str,
    pub description: &'static str,
    pub hit_die: u8,
}

impl Class {
    pub fn print_class_info(&self) {
        println!("Class {}", self.name);
        println!("{}", self.description);
        println!("Hit die: d{}", self.hit_die);
    }
}

pub fn get_class_stats(class_name: &str) -> Option<Class> {
    let mut new_class: Option<Class> = None;

    for default_class in default_classes::DEFAULT_CLASSES.iter() {
        if default_class.name.to_lowercase() == class_name.to_lowercase() {
            new_class = Some(Class {
                name: default_class.name,
                description: default_class.description,
                hit_die: default_class.hit_die,
            })
        }
    }
    new_class
}

pub fn print_default_class_info(class_name: &str) -> bool {
    let mut class_found = false;

    for default_class in default_classes::DEFAULT_CLASSES.iter() {
        if default_class.name.to_lowercase() == class_name.to_lowercase() {
            default_class.print_class_info();
            class_found = true;
        }
    }
    class_found
}
