use crate::character;
use crate::class;
use crate::race;
use crate::feature as ft;
use rand::Rng;
use std::io;

pub fn create_character() -> Option<character::Character> {
    println!("Character creator engaged!");
    println!("Let's start with your name: ");

    let mut player_name = String::new();
    io::stdin()
        .read_line(&mut player_name)
        .expect("issue reading from stdin");
    player_name = player_name.trim().to_string();

    println!("Welcome, {}. Please name your character: ", player_name);
    let mut char_name = String::new();
    io::stdin()
        .read_line(&mut char_name)
        .expect("issue reading from stdin");
    char_name = char_name.trim().to_string();

    println!("What race is {} ?", char_name);

    let mut race_name = String::new();
    io::stdin()
        .read_line(&mut race_name)
        .expect("issue reading from stdin");
    race_name = race_name.trim().to_string();

    let race_option = race::get_race_stats(&race_name);

    // TODO make this not panic
    let race = race_option.expect("Invalid race entered");

    println!("What class is {} ?", char_name);
    let mut class_name = String::new();
    io::stdin()
        .read_line(&mut class_name)
        .expect("issue reading from stdin");
    class_name = class_name.trim().to_string();

    let class_option = class::get_class_stats(&class_name);

    let class = class_option.expect("Invalid class entered");

    println!("What level is {} ?", char_name);
    let mut level = String::new();
    io::stdin()
        .read_line(&mut level)
        .expect("issue reading from stdin");
    let level: u8 = level.trim().parse().expect("Please type a number!");

    println!(
        "Welcome {}, the level {} {} {}!",
        char_name, level, race.name, class.name
    );
    println!("Now it's time for ability scores. Modifiers will be calculated for you.");
    println!("Would you like to add racial bonuses yourself? y/n");

    let mut add_bonus = String::new();
    io::stdin()
        .read_line(&mut add_bonus)
        .expect("issue reading from stdin");

    add_bonus = add_bonus.trim().to_string();

    // If the user answers "no", we'll add the racial ability bonuses for them
    let add_race_bonus_for_user = match &add_bonus[..] {
        "y" | "Y" => false,
        "n" | "N" => true,
        _ => false,
    };

    // TODO make this not terrible (i.e. use an if...let)
    let mut scores: [i8; 6] = [0; 6];
    get_ability_scores(&mut scores);

    if add_race_bonus_for_user {
        let mut score_increases: [u8; 6] = [0; 6];
        race.ability_bonus
            .get_ability_score_increases_as_array(&mut score_increases);

        for i in 0..6 {
            scores[i] = scores[i] + (score_increases[i] as i8);
        }
    }

    let abilities = character::ScoreBlock::new_score_block(&scores);

    println!("Would you like to roll for HP? (y/n)");
    println!("Otherwise, average values will be used");

    let mut roll_str = String::new();
    io::stdin()
        .read_line(&mut roll_str)
        .expect("issue reading from stdin");

    roll_str = roll_str.trim().to_string();

    let roll_for_hp: bool = match &roll_str[..] {
        "y" | "Y" => true,
        "n" | "N" => false,
        _ => false,
    };

    let hp_total = calc_max_hitpoints(&race, &class, level, abilities.con.modifier, roll_for_hp);

    println!("Now it's time for skill proficiencies. ");
    println!("Enter a comma-separated list of your skill proficiencies. ");
    println!("Type \"animal\" for animal handling and \"sleight\" for sleight of hand");

    let mut proficiencies: [bool; 18] = [false; 18];

    get_skill_proficiencies(&mut proficiencies);

    let skills = character::SkillBlock::new_blank_skill_block_with_proficiencies(&proficiencies);

    println!("");
    println!("");
    println!("");


    // Build the character!
    let new_character = character::Character::new_character(
        char_name,
        player_name,
        race,
        class,
        hp_total,
        level,
        abilities,
        skills,
    );

    println!(
        "Character built succesfully. {} is now your active character",
        new_character.get_name()
    );

    Some(new_character)
}

fn calc_max_hitpoints(
    race: &race::Race,
    class: &class::Class,
    level: u8,
    con_mod: i8,
    roll_for_hp: bool,
) -> i16 {
    let mut hp_total: i16 = 0;

    // At level one, characters have HP determined by the max roll of their hit dice
    hp_total = hp_total + (class.hit_die as i16);

    // Account for con mod
    hp_total = hp_total + (((level as i8) * con_mod) as i16);

    // Hill Dwarves get a bonus of +1hp per level
    if race.name == "Hill Dwarf" {
        hp_total = hp_total + (level as i16);
    }

    if level > 1 {
        if roll_for_hp {
            for roll in 1..level {
                let roll_amount = rand::thread_rng().gen_range(1, (class.hit_die as i16) + 1);
                hp_total = hp_total + roll_amount;
                println!("Roll at level {}: {}", roll + 1, roll_amount);
            }
        } else {
            let avg_increase: i16 = ((class.hit_die as i16) / 2) + 1;
            hp_total = hp_total + avg_increase * (level as i16 - 1);
        }
    }
    hp_total
}

// Borrow an array from the caller and fill it with the ability scores
fn get_ability_scores(score_array: &mut [i8]) {
    println!("Strength: ");
    let mut str = String::new();
    io::stdin()
        .read_line(&mut str)
        .expect("issue reading from stdin");
    score_array[0] = str.trim().parse().expect("Please type a number!");

    println!("Dexterity: ");
    let mut dex = String::new();
    io::stdin()
        .read_line(&mut dex)
        .expect("issue reading from stdin");
    score_array[1] = dex.trim().parse().expect("Please type a number!");

    println!("Constitution: ");
    let mut con = String::new();
    io::stdin()
        .read_line(&mut con)
        .expect("issue reading from stdin");
    score_array[2] = con.trim().parse().expect("Please type a number!");

    println!("Intelligence: ");
    let mut int = String::new();
    io::stdin()
        .read_line(&mut int)
        .expect("issue reading from stdin");
    score_array[3] = int.trim().parse().expect("Please type a number!");

    println!("Wisdom: ");
    let mut wis = String::new();
    io::stdin()
        .read_line(&mut wis)
        .expect("issue reading from stdin");
    score_array[4] = wis.trim().parse().expect("Please type a number!");

    println!("Charisma: ");
    let mut cha = String::new();
    io::stdin()
        .read_line(&mut cha)
        .expect("issue reading from stdin");
    score_array[5] = cha.trim().parse().expect("Please type a number!");
}

fn get_skill_proficiencies(prof_array: &mut [bool]) {
    // Expect a comma-separated list of strings as input
    // Split the array into a vector on the commas, then
    // use a match statement to set the vaues in the array

    let mut profs = String::new();
    io::stdin()
        .read_line(&mut profs)
        .expect("issue reading from stdin");

    let profs_vec: Vec<&str> = profs.trim().split(",").collect();

    for skill in &profs_vec {
        let trim_skill = skill.trim();
        match &trim_skill[..] {
            "acrobatics" => prof_array[0] = true,
            "animal" => prof_array[1] = true,
            "arcana" => prof_array[2] = true,
            "athletics" => prof_array[3] = true,
            "deception" => prof_array[4] = true,
            "history" => prof_array[5] = true,
            "insight" => prof_array[6] = true,
            "intimidation" => prof_array[7] = true,
            "investigation" => prof_array[8] = true,
            "medicine" => prof_array[9] = true,
            "nature" => prof_array[10] = true,
            "perception" => prof_array[11] = true,
            "performance" => prof_array[12] = true,
            "persuasion" => prof_array[13] = true,
            "religion" => prof_array[14] = true,
            "sleight" => prof_array[15] = true,
            "stealth" => prof_array[16] = true,
            "survival" => prof_array[17] = true,
            _ => println!("Invalid skill entered: {}", skill),
        }
    }
}
