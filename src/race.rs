use crate::default_races;
use crate::feature as ft;

#[derive(Copy, Clone)]
pub enum Size {
    Small,
    Medium,
    Large,
}

#[derive(Copy, Clone)]
pub struct AbilityScoreIncrease {
    pub str: u8,
    pub dex: u8,
    pub con: u8,
    pub int: u8,
    pub wis: u8,
    pub cha: u8,
}

impl AbilityScoreIncrease {
    pub fn print_non_zero_score_increases(&self) {
        if self.str > 0 {
            println!("Strength increase by {}", self.str);
        }
        if self.dex > 0 {
            println!("Dexterity increase by {}", self.dex);
        }
        if self.con > 0 {
            println!("Constitution increase by {}", self.con);
        }
        if self.int > 0 {
            println!("Intelligence increase by {}", self.int);
        }
        if self.wis > 0 {
            println!("Wisdom increase by {}", self.wis);
        }
        if self.cha > 0 {
            println!("Charisma increase by {}", self.cha);
        }
    }

    pub fn get_ability_score_increases_as_array(&self, score_increases: &mut [u8]) {
        score_increases[0] = self.str;
        score_increases[1] = self.dex;
        score_increases[2] = self.con;
        score_increases[3] = self.int;
        score_increases[4] = self.wis;
        score_increases[5] = self.cha;
    }
}

pub struct Race {
    pub parent_race: &'static str,
    pub name: &'static str,
    pub size: Size,
    pub speed: u8,
    pub darkvision: u8, // 0ft indicates no darkvision
    pub features: &'static [ft::Feature],
    pub ability_bonus: AbilityScoreIncrease,
}

impl Race {
    pub fn print_race_info(&self) {
        println!("{} (subrace of {})", self.name, self.parent_race);
        println!("Base speed: {}", self.speed);
        match &self.size {
            Size::Small => println!("Size: Small"),
            Size::Medium => println!("Size: Medium"),
            Size::Large => println!("Size: Large"),
        }
        println!("Darkvision: {}ft", self.darkvision);
        self.ability_bonus.print_non_zero_score_increases();

        self.print_features(); 

        println!("");
    }

    pub fn get_features(&self) -> Vec<ft::Feature> {
        let mut feature_vec: Vec<ft::Feature> = Vec::new();

        for &feature in self.features.iter() {
            feature_vec.push(feature);
        }
        feature_vec
    }

    pub fn print_features(&self) {
        println!("Race features:");
        for feature in self.features {
            feature.print_feature();
        }
    }
}

// Return none if an invalid argument is given
pub fn get_race_stats(race_name: &str) -> Option<Race> {
    let mut new_race: Option<Race> = None;

    for default_race in default_races::DEFAULT_RACES.iter() {
        if default_race.name.to_lowercase() == race_name.to_lowercase() {
            new_race = Some(Race {
                parent_race: default_race.parent_race,
                name: default_race.name,
                size: default_race.size,
                speed: default_race.speed,
                darkvision: default_race.darkvision,
                features: default_race.features,
                ability_bonus: default_race.ability_bonus,
            })
        }
    }
    new_race
}

// Return false if the race was not found
pub fn print_default_race_info(race_name: &str) -> bool {
    let mut race_found = false;
    let mut parent_race_count = 0;

    // Important: look for race first so that for single-subrace races like
    // dragonbord, tiefing, etc., we just print the subrace
    for default_race in default_races::DEFAULT_RACES.iter() {
        if default_race.name.to_lowercase() == race_name.to_lowercase() {
            default_race.print_race_info();
            race_found = true;
        } else if default_race.parent_race.to_lowercase() == race_name.to_lowercase() {
            parent_race_count = parent_race_count + 1;
            default_race.print_race_info();
            race_found = true;
        } else {
            // Nothing found, keep looking
        }
    }

    if parent_race_count > 0 {
        println!("Race {} has {} subraces", race_name, parent_race_count);
    }

    race_found
}
