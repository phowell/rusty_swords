use crate::feature as ft;
use crate::race;

pub const DEFAULT_RACES: [race::Race; 14] = [
    race::Race {
        parent_race: "dwarf",
        name: "Hill Dwarf",
        size: race::Size::Medium,
        speed: 25,
        darkvision: 60,
        features: &[
            DWARVEN_RESILIENCE_DAMAGE,
            DWARVEN_RESILIENCE_CONDITION,
            DWARVEN_TOUGHNESS,
        ],
        ability_bonus: {
            race::AbilityScoreIncrease {
                str: 0,
                dex: 0,
                con: 2,
                int: 0,
                wis: 1,
                cha: 0,
            }
        },
    },
    race::Race {
        parent_race: "dwarf",
        name: "Mountain Dwarf",
        size: race::Size::Medium,
        speed: 25,
        darkvision: 60,
        features: &[DWARVEN_RESILIENCE_DAMAGE, DWARVEN_RESILIENCE_CONDITION],
        ability_bonus: {
            race::AbilityScoreIncrease {
                str: 2,
                dex: 0,
                con: 2,
                int: 0,
                wis: 0,
                cha: 0,
            }
        },
    },
    race::Race {
        parent_race: "elf",
        name: "High Elf",
        size: race::Size::Medium,
        speed: 30,
        darkvision: 60,
        features: &[KEEN_SENSES, FEY_ANCESTRY_CHARMED, FEY_ANCESTRY_SLEEP],
        ability_bonus: {
            race::AbilityScoreIncrease {
                str: 0,
                dex: 2,
                con: 0,
                int: 1,
                wis: 0,
                cha: 0,
            }
        },
    },
    race::Race {
        parent_race: "elf",
        name: "Wood Elf",
        size: race::Size::Medium,
        speed: 35, // Fleet of foot ability
        darkvision: 60,
        features: &[KEEN_SENSES, FEY_ANCESTRY_CHARMED, FEY_ANCESTRY_SLEEP],
        ability_bonus: {
            race::AbilityScoreIncrease {
                str: 0,
                dex: 2,
                con: 0,
                int: 0,
                wis: 1,
                cha: 0,
            }
        },
    },
    race::Race {
        parent_race: "elf",
        name: "Drow",
        size: race::Size::Medium,
        speed: 30,
        darkvision: 120,
        features: &[KEEN_SENSES, FEY_ANCESTRY_CHARMED, FEY_ANCESTRY_SLEEP],
        ability_bonus: {
            race::AbilityScoreIncrease {
                str: 0,
                dex: 2,
                con: 0,
                int: 0,
                wis: 0,
                cha: 1,
            }
        },
    },
    race::Race {
        parent_race: "halfling",
        name: "Lightfoot",
        size: race::Size::Small,
        speed: 25,
        darkvision: 0,
        features: &[BRAVE],
        ability_bonus: {
            race::AbilityScoreIncrease {
                str: 0,
                dex: 2,
                con: 0,
                int: 0,
                wis: 0,
                cha: 1,
            }
        },
    },
    race::Race {
        parent_race: "halfling",
        name: "Stout",
        size: race::Size::Small,
        speed: 25,
        darkvision: 0,
        features: &[BRAVE, STOUT_RESILIENCE_CONDITION, STOUT_RESILIENCE_DAMAGE],
        ability_bonus: {
            race::AbilityScoreIncrease {
                str: 0,
                dex: 2,
                con: 1,
                int: 0,
                wis: 0,
                cha: 0,
            }
        },
    },
    race::Race {
        parent_race: "human",
        name: "Human",
        size: race::Size::Medium,
        speed: 30,
        darkvision: 0,
        features: &[],
        ability_bonus: {
            race::AbilityScoreIncrease {
                str: 1,
                dex: 1,
                con: 1,
                int: 1,
                wis: 1,
                cha: 1,
            }
        },
    },
    race::Race {
        parent_race: "dragonborn",
        name: "Dragonborn",
        size: race::Size::Medium,
        speed: 30,
        darkvision: 0,
        features: &[],
        ability_bonus: {
            race::AbilityScoreIncrease {
                str: 2,
                dex: 0,
                con: 0,
                int: 0,
                wis: 0,
                cha: 1,
            }
        },
    },
    race::Race {
        parent_race: "gnome",
        name: "Forest Gnome",
        size: race::Size::Small,
        speed: 25,
        darkvision: 60,
        features: &[GNOME_CUNNING],
        ability_bonus: {
            race::AbilityScoreIncrease {
                str: 0,
                dex: 1,
                con: 0,
                int: 2,
                wis: 0,
                cha: 0,
            }
        },
    },
    race::Race {
        parent_race: "gnome",
        name: "Rock Gnome",
        size: race::Size::Small,
        speed: 25,
        darkvision: 60,
        features: &[GNOME_CUNNING],
        ability_bonus: {
            race::AbilityScoreIncrease {
                str: 0,
                dex: 0,
                con: 1,
                int: 2,
                wis: 0,
                cha: 0,
            }
        },
    },
    race::Race {
        parent_race: "half-elf",
        name: "Half-Elf",
        size: race::Size::Medium,
        speed: 30,
        darkvision: 60,
        features: &[FEY_ANCESTRY_CHARMED, FEY_ANCESTRY_SLEEP],
        ability_bonus: {
            race::AbilityScoreIncrease {
                str: 0,
                dex: 0,
                con: 0,
                int: 0,
                wis: 0,
                cha: 2,
            }
        },
    },
    race::Race {
        parent_race: "half-orc",
        name: "Half-Orc",
        size: race::Size::Medium,
        speed: 30,
        darkvision: 60,
        features: &[MENACING],
        ability_bonus: {
            race::AbilityScoreIncrease {
                str: 2,
                dex: 0,
                con: 1,
                int: 0,
                wis: 0,
                cha: 0,
            }
        },
    },
    race::Race {
        parent_race: "tiefling",
        name: "Tiefling",
        size: race::Size::Medium,
        speed: 30,
        darkvision: 60,
        features: &[HELLISH_RESISTANCE],
        ability_bonus: {
            race::AbilityScoreIncrease {
                str: 0,
                dex: 0,
                con: 0,
                int: 1,
                wis: 0,
                cha: 2,
            }
        },
    },
];

// Generic dwarf
const DWARVEN_RESILIENCE_CONDITION: ft::Feature = ft::Feature {
    feature_name: "Dwarven Resilience",
    tags: &[ft::FeatureTag::Condition],
    feature_type: ft::FeatureType::Passive,
    description: "Advantage on saving throws against being poisoned",
    roll_mod: Some(ft::RollMod::Advantage),
    die_roll: None,
    flat_increase: None,
    increase_per_level: None,
    uses: 0,
    uses_reset: None,
    affects_ability: None,
    condition_or_damage_mod: None,
    affects_damage_type: None,
    affects_condition: Some(ft::Condition::Poisoned),
    affects_skill: None,
};

// Generic dwarf
const DWARVEN_RESILIENCE_DAMAGE: ft::Feature = ft::Feature {
    feature_name: "Dwarven Resilience",
    tags: &[ft::FeatureTag::DamageType],
    feature_type: ft::FeatureType::Passive,
    description: "Resistance to poison damage",
    roll_mod: None,
    die_roll: None,
    flat_increase: None,
    increase_per_level: None,
    uses: 0,
    uses_reset: None,
    affects_ability: None,
    condition_or_damage_mod: Some(ft::ConditionOrDamageTypeMod::Resist),
    affects_damage_type: Some(ft::DamageType::Poison),
    affects_condition: None,
    affects_skill: None,
};

// Mountain Dwarf


// Hill Dwarf
const DWARVEN_TOUGHNESS: ft::Feature = ft::Feature {
    feature_name: "Dwarven Toughness",
    tags: &[ft::FeatureTag::HitPoints],
    feature_type: ft::FeatureType::Passive,
    description: "One extra HP per level",
    roll_mod: None,
    die_roll: None,
    flat_increase: None,
    increase_per_level: Some(1),
    uses: 0,
    uses_reset: None,
    affects_ability: None,
    condition_or_damage_mod: None,
    affects_damage_type: None,
    affects_condition: None,
    affects_skill: None,
};

// Generic Elf
const KEEN_SENSES: ft::Feature = ft::Feature {
    feature_name: "Keen Senses",
    tags: &[ft::FeatureTag::SkillProficiency],
    feature_type: ft::FeatureType::Passive,
    description: "Proficiency in Perception",
    roll_mod: None,
    die_roll: None,
    flat_increase: None,
    increase_per_level: None,
    uses: 0,
    uses_reset: None,
    affects_ability: None,
    condition_or_damage_mod: None,
    affects_damage_type: None,
    affects_condition: None,
    affects_skill: Some(ft::Skill::Perception),
};

// Generic Elf, Half-Elf
const FEY_ANCESTRY_CHARMED: ft::Feature = ft::Feature {
    feature_name: "Fey Ancestry",
    tags: &[ft::FeatureTag::Condition],
    feature_type: ft::FeatureType::Passive,
    description: "Advantage against being charmed",
    roll_mod: Some(ft::RollMod::Advantage),
    die_roll: None,
    flat_increase: None,
    increase_per_level: None,
    uses: 0,
    uses_reset: None,
    affects_ability: None,
    condition_or_damage_mod: None,
    affects_damage_type: None,
    affects_condition: Some(ft::Condition::Charmed),
    affects_skill: None,
};

// Generic Elf, Half-Elf
const FEY_ANCESTRY_SLEEP: ft::Feature = ft::Feature {
    feature_name: "Fey Ancestry",
    tags: &[ft::FeatureTag::Condition],
    feature_type: ft::FeatureType::Passive,
    description: "Immune to magic sleep",
    roll_mod: None,
    die_roll: None,
    flat_increase: None,
    increase_per_level: None,
    uses: 0,
    uses_reset: None,
    affects_ability: None,
    condition_or_damage_mod: Some(ft::ConditionOrDamageTypeMod::Immune),
    affects_damage_type: None,
    affects_condition: Some(ft::Condition::Sleep),
    affects_skill: None,
};

// High Elf

// Woof Elf

// Drow

// Generic Halfling
const BRAVE: ft::Feature = ft::Feature {
    feature_name: "Brave",
    tags: &[ft::FeatureTag::Condition],
    feature_type: ft::FeatureType::Passive,
    description: "Advantage against being frightened",
    roll_mod: Some(ft::RollMod::Advantage),
    die_roll: None,
    flat_increase: None,
    increase_per_level: None,
    uses: 0,
    uses_reset: None,
    affects_ability: None,
    condition_or_damage_mod: None,
    affects_damage_type: None,
    affects_condition: Some(ft::Condition::Frightened),
    affects_skill: None,
};

// Lightfoot

// Stout
const STOUT_RESILIENCE_CONDITION: ft::Feature = ft::Feature {
    feature_name: "Stout Resilience",
    tags: &[ft::FeatureTag::Condition],
    feature_type: ft::FeatureType::Passive,
    description: "Advantage on saving throws against being poisoned",
    roll_mod: Some(ft::RollMod::Advantage),
    die_roll: None,
    flat_increase: None,
    increase_per_level: None,
    uses: 0,
    uses_reset: None,
    affects_ability: None,
    condition_or_damage_mod: None,
    affects_damage_type: None,
    affects_condition: Some(ft::Condition::Poisoned),
    affects_skill: None,
};

// Stout
const STOUT_RESILIENCE_DAMAGE: ft::Feature = ft::Feature {
    feature_name: "Stout Resilience",
    tags: &[ft::FeatureTag::DamageType],
    feature_type: ft::FeatureType::Passive,
    description: "Resistance to poison damage",
    roll_mod: None,
    die_roll: None,
    flat_increase: None,
    increase_per_level: None,
    uses: 0,
    uses_reset: None,
    affects_ability: None,
    condition_or_damage_mod: Some(ft::ConditionOrDamageTypeMod::Resist),
    affects_damage_type: Some(ft::DamageType::Poison),
    affects_condition: None,
    affects_skill: None,
};

// Human

// Dragonborn
// TODO figure out the dragonborn breath weapon

// Generic Gnome
const GNOME_CUNNING: ft::Feature = ft::Feature {
    feature_name: "Gnome Cunning",
    tags: &[ft::FeatureTag::SavingThrow],
    feature_type: ft::FeatureType::Passive,
    description: "Advantage on Int, Wis, and Cha saving throws against magic",
    roll_mod: Some(ft::RollMod::Advantage),
    die_roll: None,
    flat_increase: None,
    increase_per_level: None,
    uses: 0,
    uses_reset: None,
    affects_ability: Some(&[ft::Ability::Int, ft::Ability::Wis, ft::Ability::Cha]),
    condition_or_damage_mod: None,
    affects_damage_type: None,
    affects_condition: None,
    affects_skill: None,
};

// Forest Gnome

// Rock Gnome

// Half-Orc
const MENACING: ft::Feature = ft::Feature {
    feature_name: "Menacing",
    tags: &[ft::FeatureTag::SkillProficiency],
    feature_type: ft::FeatureType::Passive,
    description: "Proficiency in Intimidation",
    roll_mod: None,
    die_roll: None,
    flat_increase: None,
    increase_per_level: None,
    uses: 0,
    uses_reset: None,
    affects_ability: None,
    condition_or_damage_mod: None,
    affects_damage_type: None,
    affects_condition: None,
    affects_skill: Some(ft::Skill::Intimidation),
};

// Tiefling
const HELLISH_RESISTANCE: ft::Feature = ft::Feature {
    feature_name: "Hellish Resistance",
    tags: &[ft::FeatureTag::DamageType],
    feature_type: ft::FeatureType::Passive,
    description: "Resistance to fire damage",
    roll_mod: None,
    die_roll: None,
    flat_increase: None,
    increase_per_level: None,
    uses: 0,
    uses_reset: None,
    affects_ability: None,
    condition_or_damage_mod: Some(ft::ConditionOrDamageTypeMod::Resist),
    affects_damage_type: Some(ft::DamageType::Fire),
    affects_condition: None,
    affects_skill: None,
};
