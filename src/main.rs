// TODO this supresses dead code warnings crate-wide
#![allow(dead_code)]

use std::io;

// Each of these is its own file, in this folder
mod character;
mod class;
mod creator;
mod default_classes;
mod default_races;
mod feature;
mod levels;
mod race;

fn main() {
    let mut active_char: Option<character::Character> = None;

    print_welcome_message();

    loop {
        print_main_menu();

        let mut input = String::new();
        io::stdin()
            .read_line(&mut input)
            .expect("issue reading from stdin");

        // Slice of any newlines or whitespaces
        input = input.trim().to_string();

        match &input[..] {
            "create" => active_char = creator::create_character(),
            "load" => load_character(),
            "active" => match &active_char {
                None => println!("No active character"),
                Some(character) => character.print_character(),
            },
            "info" => info_menu(),
            "quit" => {
                println!("Goodbye!");
                break;
            }
            _ => println!("Invalid input, try again"),
        }
    }
}

fn print_welcome_message() {
    println!("Welcome to Rusty Swords, a D&D character creator and playing tool written in rust");
    println!("By Patrick Howell");
}

fn print_main_menu() {
    println!("");
    println!("You are at the main menu. Your options are: ");
    println!("create - create a new character");
    println!("load - load a character by name");
    println!("active - print description of your active character");
    println!("info - print info on races, classes, etc");
    println!("quit - quit the application");
}

fn info_menu() {
    loop {
        print_info_menu();

        let mut input = String::new();
        io::stdin()
            .read_line(&mut input)
            .expect("issue reading from stdin");

        // Slice of any newlines or whitespaces
        input = input.trim().to_string();

        match &input[..] {
            "race" => race_info_menu(),
            "class" => class_info_menu(),
            "back" => break,
            "help" => print_info_menu(),
            _ => println!("Invalid input, try again"),
        }
    }
}

fn print_info_menu() {
    println!("");
    println!("You are at the info menu. Your options are:");
    println!("race - get info on a specific race");
    println!("class - get info on a specific class");
    println!("back - return to main menu");
    println!("help - print this menu");
}

fn race_info_menu() {
    println!("");
    println!("Race info menu: type the name of the race you would like to know more about");
    println!("Type back to return to the info menu");

    loop {
        let mut input = String::new();
        io::stdin()
            .read_line(&mut input)
            .expect("issue reading from stdin");
        input = input.trim().to_string();

        if input == "back" {
            break;
        }

        if race::print_default_race_info(&input) == false {
            println!("Error: race not found");
        }
        println!("");
        println!("Enter another race or type back to return to the info menu")
    }
}

fn class_info_menu() {
    println!("");
    println!("Class info menu: type the name of the class you would like to know more about");
    println!("Type back to return to the info menu");

    loop {
        let mut input = String::new();
        io::stdin()
            .read_line(&mut input)
            .expect("issue reading from stdin");
        input = input.trim().to_string();

        if input == "back" {
            break;
        }

        if class::print_default_class_info(&input) == false {
            println!("Error: class not found");
        }
        println!("");
        println!("Enter another class or type back to return to the info menu")
    }
}

fn load_character() {
    println!("Please enter the name of the character to load: ");
}

// fn test_character() {
// 	let name = String::from("Giblett");
// 	let player_name = String::from("Patrick");
// 	let class = String::from("Bard/Sorcerer");
// 	let level = 6;
// 	let race = String::from("Rock Gnome");
// 	let speed = 25;
//
// 	let scores = [8,14, 11, 16, 12, 16];
// 	let abilities = character::ScoreBlock::new_score_block(&scores);
// 	let skills_profs = [true, false, false, false, false, true, false, false, false, false, false, false, true, true, false, true, true, false];
// 	let skills = character::SkillBlock::new_blank_skill_block_with_proficiencies(&skills_profs);
//
// 	let mut my_character = character::Character::new_character(name, player_name, race, class, level, speed, abilities, skills);
//
// 	my_character.print_character();
// }
