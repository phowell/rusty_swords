#[derive(Copy, Clone, PartialEq)]
pub enum FeatureTag {
    AttackRoll,
    DamageRoll,
    SavingThrow,
    AbilityMod,
    SkillCheck,
    InitiativeRoll,
    Speed,
    HitPoints,
    Condition,
    DamageType,
    ExtraAttack,
    ShortRest,
    LongRest,
    WeaponProficiency,
    ToolProficiency,
    ArmourProficiency,
    SkillProficiency,
}

#[derive(Copy, Clone, PartialEq)]
pub enum RollMod {
    Advantage,
    Disadvantage,
}

#[derive(Copy, Clone, PartialEq)]
pub enum ResetFeatureUses {
    ShortRest,
    LongRest,
    Daily,
    Other,
}

#[derive(Copy, Clone, PartialEq)]
pub enum ConditionOrDamageTypeMod {
    Vuln,
    Resist,
    Immune,
}

#[derive(Copy, Clone, PartialEq)]
pub enum FeatureType {
    Action,
    Reaction,
    BonusAction,
    Passive,
}

#[derive(Copy, Clone, PartialEq)]
pub enum DamageType {
    Acid,
    Bludgeoning,
    Cold,
    Fire,
    Force,
    Lightning,
    Necrotic,
    Piercing,
    Poison,
    Pyschic,
    Radiant,
    Slashing,
    Thunder,
}

#[derive(Copy, Clone, PartialEq)]
pub enum Condition {
    Blinded,
    Charmed,
    Deafened,
    Frightened,
    Grappled,
    Incapacitated,
    Invisible,
    Paralyzed,
    Petrified,
    Poisoned,
    Prone,
    Restrained,
    Sleep,
    Stunned,
    Unconscious,
}

#[derive(Copy, Clone, PartialEq)]
pub enum Ability {
    Str,
    Dex,
    Con,
    Int,
    Wis,
    Cha,
}

#[derive(Copy, Clone, PartialEq)]
pub enum Skill {
    Acrobatics,
    AnimalHandling,
    Arcana,
    Athletics,
    Deception,
    History,
    Insight,
    Intimidation,
    Investigation,
    Medicine,
    Nature,
    Perception,
    Performance,
    Persuasion,
    Religion,
    SleightOfHand,
    Stealth,
    Survival,
}

// A feature is provided either by a race or a class
// Class should attach each feature to a level
#[derive(Copy, Clone)]
pub struct Feature {
    // Give the feature a printable name
    pub feature_name: &'static str,
    // Tag the feature so that the feature parser knows when to use it
    pub tags: &'static [FeatureTag],
    // Does the feature activate on an action, bonus action, reaction, or is it passive (always active)
    pub feature_type: FeatureType,
    // A printable description of the feature
    pub description: &'static str,
    // If not none, does the feature provide Advantage or Disadvantage
    pub roll_mod: Option<RollMod>,
    // If not none, what die (d4, d6, d8, etc) does the feature add to the role
    pub die_roll: Option<u8>,
    // If not none, what flat mod does the feature provide
    pub flat_increase: Option<i8>,
    // If not none, what flat increase per level does the feature provide
    pub increase_per_level: Option<i8>,
    // How many times per reset can the feature be used, 0 for unlimited
    pub uses: u8,
    // If not none, what is the condition to reset the number of feature uses
    pub uses_reset: Option<ResetFeatureUses>,
    // If not none, what abilities does the feature apply to
    pub affects_ability: Option<&'static [Ability]>,
    // If not none, does the feature provide vulnerability, resistance, or immunity to a damage type or condition
    pub condition_or_damage_mod: Option<ConditionOrDamageTypeMod>,
    // If not none, what damage type does the feature affect
    pub affects_damage_type: Option<DamageType>,
    // If not none, what condition does the feature affect
    pub affects_condition: Option<Condition>,
    // If not none, what skill does the feature affect
    pub affects_skill: Option<Skill>,
}

impl Feature {
    pub fn print_feature(&self) {
        println!("{}: {}", self.feature_name, self.description);
    }

    pub fn does_feature_modify_saving_roll_condition(&self, condition: Condition) -> Option<RollMod> {
        let mut roll_mod: Option<RollMod> = None;

        for &tag in self.tags.iter()
        {
            if tag == FeatureTag::SavingThrow {
                if let Some(cond) = self.affects_condition {
                    if cond == condition {
                        roll_mod = self.roll_mod;
                        return roll_mod;
                    }
                }
            }
        }
        roll_mod
    }

    pub fn does_feature_modify_saving_roll_ability(&self, ability_to_check: Ability) -> Option<RollMod> {
        let mut roll_mod: Option<RollMod> = None;

        for &tag in self.tags.iter() {
            if tag == FeatureTag::SavingThrow {
                if let Some(ability_array) = self.affects_ability {
                    for &ability in ability_array.iter() {
                        if ability == ability_to_check {
                            roll_mod = self.roll_mod;
                            return roll_mod;
                        }

                    }
                }
            }
        }
        roll_mod
    }

    pub fn does_feature_modify_hp(&self, level: u8) -> i8 {
        let mut hp_mod: i8 = 0;
        
        for &tag in self.tags.iter() {
            if tag == FeatureTag::HitPoints {
                // Check for a flat increase to HP
                if let Some(flat_increase) = self.flat_increase {
                    hp_mod = hp_mod + flat_increase;
                }

                // Check for a per-level increase to hp
                if let Some(per_level) = self.increase_per_level {
                    hp_mod = hp_mod + (level as i8) * per_level;
                }
            }
        }
        hp_mod
    }
} 
