use crate::class;
use crate::race;
use crate::feature as ft;

pub struct AbilityScore {
    pub score: i8,
    pub modifier: i8,
}

impl AbilityScore {
    pub fn new_ability_score(score: i8) -> AbilityScore {
        AbilityScore {
            score: score,
            modifier: (score - 10) / 2,
        }
    }
}

pub struct ScoreBlock {
    pub str: AbilityScore,
    pub dex: AbilityScore,
    pub con: AbilityScore,
    pub int: AbilityScore,
    pub wis: AbilityScore,
    pub cha: AbilityScore,
}

impl ScoreBlock {
    pub fn new_score_block(score_array: &[i8]) -> ScoreBlock {
        ScoreBlock {
            str: AbilityScore::new_ability_score(score_array[0]),
            dex: AbilityScore::new_ability_score(score_array[1]),
            con: AbilityScore::new_ability_score(score_array[2]),
            int: AbilityScore::new_ability_score(score_array[3]),
            wis: AbilityScore::new_ability_score(score_array[4]),
            cha: AbilityScore::new_ability_score(score_array[5]),
        }
    }

    pub fn print_score_block(&self) {
        println!("Ability Scores (and modifiers) for this character:");
        println!("Stength: {} ({})", self.str.score, self.str.modifier);
        println!("Dexterity: {} ({})", self.dex.score, self.dex.modifier);
        println!("Constitution: {} ({})", self.con.score, self.con.modifier);
        println!("Intelligence: {} ({})", self.int.score, self.int.modifier);
        println!("Wisdom: {} ({})", self.wis.score, self.wis.modifier);
        println!("Charisma: {} ({})", self.cha.score, self.cha.modifier);
    }
}

// Modifier is including proficiency bonus
pub struct Skill {
    modifier: i8,
    proficiency: bool,
}

impl Skill {
    pub fn new_skill(modifier: i8, proficiency: bool) -> Skill {
        Skill {
            modifier,
            proficiency,
        }
    }
}

pub struct SkillBlock {
    acrobatics: Skill,
    animal_handling: Skill,
    arcana: Skill,
    athletics: Skill,
    deception: Skill,
    history: Skill,
    insight: Skill,
    intimidation: Skill,
    investigation: Skill,
    medicine: Skill,
    nature: Skill,
    perception: Skill,
    performance: Skill,
    persuasion: Skill,
    religion: Skill,
    sleight_of_hand: Skill,
    stealth: Skill,
    survival: Skill,
}

impl SkillBlock {
    pub fn new_blank_skill_block_with_proficiencies(skill_profs: &[bool]) -> SkillBlock {
        SkillBlock {
            acrobatics: Skill::new_skill(0, skill_profs[0]),
            animal_handling: Skill::new_skill(0, skill_profs[1]),
            arcana: Skill::new_skill(0, skill_profs[2]),
            athletics: Skill::new_skill(0, skill_profs[3]),
            deception: Skill::new_skill(0, skill_profs[4]),
            history: Skill::new_skill(0, skill_profs[5]),
            insight: Skill::new_skill(0, skill_profs[6]),
            intimidation: Skill::new_skill(0, skill_profs[7]),
            investigation: Skill::new_skill(0, skill_profs[8]),
            medicine: Skill::new_skill(0, skill_profs[9]),
            nature: Skill::new_skill(0, skill_profs[10]),
            perception: Skill::new_skill(0, skill_profs[11]),
            performance: Skill::new_skill(0, skill_profs[12]),
            persuasion: Skill::new_skill(0, skill_profs[13]),
            religion: Skill::new_skill(0, skill_profs[14]),
            sleight_of_hand: Skill::new_skill(0, skill_profs[15]),
            stealth: Skill::new_skill(0, skill_profs[16]),
            survival: Skill::new_skill(0, skill_profs[17]),
        }
    }

    pub fn print_skill_block(&self) {
        println!("Skills for this character:");
        println!(
            "Acrobatics (DEX): {}, proficient: {}",
            self.acrobatics.modifier, self.acrobatics.proficiency
        );
        println!(
            "Animal Handling (WIS): {}, proficient: {}",
            self.animal_handling.modifier, self.animal_handling.proficiency
        );
        println!(
            "Arcana (INT): {}, proficient: {}",
            self.arcana.modifier, self.arcana.proficiency
        );
        println!(
            "Athletics (STR): {}, proficient: {}",
            self.athletics.modifier, self.athletics.proficiency
        );
        println!(
            "Deception (CHA): {}, proficient: {}",
            self.deception.modifier, self.deception.proficiency
        );
        println!(
            "History (INT): {}, proficient: {}",
            self.history.modifier, self.history.proficiency
        );
        println!(
            "Insight (WIS): {}, proficient: {}",
            self.insight.modifier, self.insight.proficiency
        );
        println!(
            "Intimidation (CHA): {}, proficient: {}",
            self.intimidation.modifier, self.intimidation.proficiency
        );
        println!(
            "Investigation (INT): {}, proficient: {}",
            self.investigation.modifier, self.investigation.proficiency
        );
        println!(
            "Medicine (WIS): {}, proficient: {}",
            self.medicine.modifier, self.medicine.proficiency
        );
        println!(
            "Nature (INT): {}, proficient: {}",
            self.nature.modifier, self.nature.proficiency
        );
        println!(
            "Perception (WIS): {}, proficient: {}",
            self.perception.modifier, self.perception.proficiency
        );
        println!(
            "Performance (CHA): {}, proficient: {}",
            self.performance.modifier, self.performance.proficiency
        );
        println!(
            "Persuasion (CHA) : {}, proficient: {}",
            self.persuasion.modifier, self.persuasion.proficiency
        );
        println!(
            "Religion (INT): {}, proficient: {}",
            self.religion.modifier, self.religion.proficiency
        );
        println!(
            "Sleight of Hand (DEX): {}, proficient: {}",
            self.sleight_of_hand.modifier, self.sleight_of_hand.proficiency
        );
        println!(
            "Stealth (DEX): {}, proficient: {}",
            self.stealth.modifier, self.stealth.proficiency
        );
        println!(
            "Survival (WIS) : {}, proficient: {}",
            self.survival.modifier, self.survival.proficiency
        );
    }
}

pub struct Character {
    name: String,
    player_name: String,
    race: race::Race,
    class: class::Class,
    max_hp: i16,
    current_hp: i16,
    level: u8,
    prof_bonus: u8,
    initiative: i8,
    score_block: ScoreBlock,
    skill_block: SkillBlock,
}

impl Character {
    pub fn new_character(
        name: String,
        player_name: String,
        race: race::Race,
        class: class::Class,
        max_hp: i16,
        level: u8,
        abilities: ScoreBlock,
        proficiencies: SkillBlock,
    ) -> Character {
        let mut new_character = Character {
            name: name,
            player_name: player_name,
            race: race,
            max_hp: max_hp,
            current_hp: max_hp,
            class: class,
            level: level,
            prof_bonus: Character::prof_bonus_helper(level),
            initiative: abilities.dex.modifier,
            score_block: abilities,
            skill_block: proficiencies,
        };

        new_character.derive_skill_mods();

        new_character
    }

    pub fn print_character(&self) {
        println!("");
        println!(
            "Character: {}, A level {} {} {} played by {}",
            self.name, self.level, self.race.name, self.class.name, self.player_name
        );
        println!("Proficiency Bonus: {}", self.prof_bonus);
        println!("Initiative: {}", self.initiative);
        println!("Speed: {}ft", self.race.speed);
        println!("HP: {}/{}", self.current_hp, self.max_hp);
        println!("***********************************************************************************************************************");
        self.score_block.print_score_block();
        println!("***********************************************************************************************************************");
        self.skill_block.print_skill_block();
        println!("");
        self.race.print_features();
    }

    pub fn get_name(&self) -> String {
        let mut name = String::new();
        name.push_str(&self.name);
        name
    }

    fn prof_bonus_helper(level: u8) -> u8 {
        if level <= 5 {
            2
        } else if level >= 6 && level <= 8 {
            3
        } else if level >= 9 && level <= 12 {
            4
        } else if level >= 13 && level <= 16 {
            5
        } else if level >= 17 && level <= 20 {
            6
        } else {
            0
        }
    }

    fn derive_skill_mods(&mut self) {
        self.skill_block.acrobatics.modifier = Character::derive_skill_helper(
            self.score_block.dex.modifier,
            self.skill_block.acrobatics.proficiency,
            self.prof_bonus,
        );
        self.skill_block.animal_handling.modifier = Character::derive_skill_helper(
            self.score_block.wis.modifier,
            self.skill_block.animal_handling.proficiency,
            self.prof_bonus,
        );
        self.skill_block.arcana.modifier = Character::derive_skill_helper(
            self.score_block.int.modifier,
            self.skill_block.arcana.proficiency,
            self.prof_bonus,
        );
        self.skill_block.athletics.modifier = Character::derive_skill_helper(
            self.score_block.str.modifier,
            self.skill_block.athletics.proficiency,
            self.prof_bonus,
        );
        self.skill_block.deception.modifier = Character::derive_skill_helper(
            self.score_block.cha.modifier,
            self.skill_block.deception.proficiency,
            self.prof_bonus,
        );
        self.skill_block.history.modifier = Character::derive_skill_helper(
            self.score_block.int.modifier,
            self.skill_block.history.proficiency,
            self.prof_bonus,
        );
        self.skill_block.insight.modifier = Character::derive_skill_helper(
            self.score_block.wis.modifier,
            self.skill_block.insight.proficiency,
            self.prof_bonus,
        );
        self.skill_block.intimidation.modifier = Character::derive_skill_helper(
            self.score_block.cha.modifier,
            self.skill_block.intimidation.proficiency,
            self.prof_bonus,
        );
        self.skill_block.investigation.modifier = Character::derive_skill_helper(
            self.score_block.int.modifier,
            self.skill_block.investigation.proficiency,
            self.prof_bonus,
        );
        self.skill_block.medicine.modifier = Character::derive_skill_helper(
            self.score_block.wis.modifier,
            self.skill_block.medicine.proficiency,
            self.prof_bonus,
        );
        self.skill_block.nature.modifier = Character::derive_skill_helper(
            self.score_block.int.modifier,
            self.skill_block.nature.proficiency,
            self.prof_bonus,
        );
        self.skill_block.perception.modifier = Character::derive_skill_helper(
            self.score_block.cha.modifier,
            self.skill_block.perception.proficiency,
            self.prof_bonus,
        );
        self.skill_block.performance.modifier = Character::derive_skill_helper(
            self.score_block.cha.modifier,
            self.skill_block.performance.proficiency,
            self.prof_bonus,
        );
        self.skill_block.persuasion.modifier = Character::derive_skill_helper(
            self.score_block.cha.modifier,
            self.skill_block.persuasion.proficiency,
            self.prof_bonus,
        );
        self.skill_block.religion.modifier = Character::derive_skill_helper(
            self.score_block.int.modifier,
            self.skill_block.religion.proficiency,
            self.prof_bonus,
        );
        self.skill_block.sleight_of_hand.modifier = Character::derive_skill_helper(
            self.score_block.dex.modifier,
            self.skill_block.sleight_of_hand.proficiency,
            self.prof_bonus,
        );
        self.skill_block.stealth.modifier = Character::derive_skill_helper(
            self.score_block.dex.modifier,
            self.skill_block.stealth.proficiency,
            self.prof_bonus,
        );
        self.skill_block.survival.modifier = Character::derive_skill_helper(
            self.score_block.wis.modifier,
            self.skill_block.survival.proficiency,
            self.prof_bonus,
        );
    }

    fn derive_skill_helper(skill_mod: i8, proficiency: bool, prof_bonus: u8) -> i8 {
        if proficiency {
            skill_mod + (prof_bonus as i8)
        } else {
            skill_mod
        }
    }
}
